<?php
/**
 * OCP.
 * @copyright Copyright (c) Sigma Software
 * @package   solid
 * @author    Alexey Petrenko <alex.petrenko@sigmaukraine.com>
 */
namespace ocp\principe;

interface TradePlace {
    public function sale();
    public function pack();

}

class Picture {

    public $picture;
    public $materials;

    public function __construct($materials)
    {
        $this->materials = $materials;
        $this->create();
    }

    public function get()
    {
        return $this->picture;
    }

    public function create()
    {
        $this->picture = 'picture painted with ' . $this->materials;
    }
}

class Gallery implements TradePlace
{

    private $_picture;

    public function __construct($picture)
    {
        $this->_picture = $picture;
    }

    public function pack()
    {
        return 'picture packed';
    }

    public function sale()
    {
        return 'picture ' . $this->_picture->name . ' sold';
    }
}

class Auction implements TradePlace
{
    private $_picture;
    private $_endPrice;

    public function __construct($picture)
    {
        $this->_picture = $picture;
    }

    public function pack()
    {
        return 'picture packed';
    }

    public function bid($money)
    {
        $this->_endPrice += $money;
    }
    public function sale()
    {
        return 'picture ' . $this->_picture->name . ' sold. Price was ' . $this->_endPrice;
    }
}

class Buyer {

    private $_tradePlace;
    public function __construct(TradePlace $tradePlace)
    {
        $this->_tradePlace = $tradePlace;
    }

    public function buy()
    {
        return $this->_tradePlace->sale();
    }
}
