<?php
/**
 * DIP.
 * @copyright Copyright (c) Sigma Software
 * @package   solid
 * @author    Alexey Petrenko <alex.petrenko@sigmaukraine.com>
 */

class Assorntment
{
    public function __construct()
    {
        return 'electronics';
    }
}

class Assortment2
{
    public function __construct()
    {
        return 'food';
    }
}

class BadMarket
{
    private $assortment;
    public function __construct()
    {
        $this->assortment = new Assorntment();
    }
}

class SuperMarket
{
    private $assortment;
    public function __construct($assortment)
    {
        $this->assortment = $assortment;
    }
}

