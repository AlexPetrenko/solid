<?php
/**
 * Description.
 * @copyright Copyright (c) Sigma Software
 * @package   solid
 * @author    Alexey Petrenko <alex.petrenko@sigmaukraine.com>
 */
interface IFoodProcessor
{
    public function grind();
    public function blend();
    public function slice();
}

class FoodProcessor implements  IFoodProcessor
{
    public function grind()
    {
        return 'Grind food';
    }
    public function blend()
    {
        return 'Blend food';
    }
    public function slice()
    {
        return 'Slice food';
    }

}

class Blender implements IFoodProcessor
{
    public function grind()
    {
        return new Exception('I can\'t chop');
    }
    public function blend()
    {
        return 'Blend food';
    }
    public function slice()
    {
        return new Exception('I can\'t slice');
    }
}

class BladeGrinder implements IFoodProcessor
{
    public function grind()
    {
        return 'Grind food';
    }
    public function blend()
    {
        return new Exception('I can\'t blend');
    }
    public function slice()
    {
        return new Exception('I can\'t slice');
    }
}

interface IBlender
{
    public function blend();
}

interface ISlicer
{
    public function slice();
}

interface IGrinder
{
    public function grind();
}
