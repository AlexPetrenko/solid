<?php
/**
 * LSP.
 * @copyright Copyright (c) Sigma Software
 * @package   solid
 * @author    Alexey Petrenko <alex.petrenko@sigmaukraine.com>
 */
class Image {

    public function draw()
    {
        return '<img class="image" src="bla.jpg">';
    }
}

class WrongImage extends Image {
    public function draw()
    {
        return '<div>BLa</div>';
    }

}

class OkImage extends Image {
    public function draw()
    {
        return '<svg><rect height="20" width="20"></rect></svg>';
    }

}