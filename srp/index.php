<?php
/**
 * SRP.
 * @copyright Copyright (c) Myself
 * @package   solid
 * @author    Alexey Petrenko <alex.petrenko@sigmaukraine.com>
 */
namespace srp\principe;

class Picture {

    public $picture;
    public $materials;

    public function __construct($materials)
    {
        $this->materials = $materials;
        $this->paint();
    }

    public function getPicture()
    {
        return $this->picture;
    }

    public function paint()
    {
        $this->picture = 'picture painted with ' . $this->materials;
    }

    public function sale()
    {
        return 'picture ' . $this->picture->name . ' sold';
    }

    public function pack()
    {
        return 'picture packed';
    }
}

class PictureSingle {

    public $picture;
    public $materials;

    public function __construct($materials)
    {
        $this->materials = $materials;
        $this->paint();
    }

    public function getPicture()
    {
       return $this->picture;
    }

    public function paint()
    {
        $this->picture = 'picture painted with ' . $this->materials;
    }
}

class Gallery {

    private $_picture;

    public function __construct($picture)
    {
        $this->_picture = $picture;
    }

    public function pack()
    {
        return 'picture packed';
    }

    public function sale()
    {
        return $this->_picture . ' sold';
    }
}

$picture = new PictureSingle('paint, canvas');
$gallery = new Gallery($picture->getPicture());
$gallery->sale();